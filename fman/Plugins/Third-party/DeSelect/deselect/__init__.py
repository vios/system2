from fman import DirectoryPaneCommand
from fman.url import as_url, join
from fman.fs import iterdir


class Deselect(DirectoryPaneCommand):
    def __call__(self):
        selected_files = self.pane.get_selected_files()
        if len(selected_files) >= 1 or (len(selected_files) == 0 and self.get_chosen_files()):
            if len(selected_files) == 0 and self.get_chosen_files():
                selected_files.append(self.get_chosen_files()[0])
            #
            # Loop through each file/directory selected.
            #
            for filedir in selected_files:
                self.pane.toggle_selection(filedir)

class InvertAll(DirectoryPaneCommand):
    def __call__(self):
        dirPath = self.pane.get_path()
        for item in iterdir(dirPath):
            try:
                self.pane.toggle_selection(join(dirPath,item))
            except ValueError:
               tmp = 0 
