## DeSelect

Plugin for [fman.io](https://fman.io) to remove the selection from the currently selected files and directories.

You can install this plugin by pressing `<shift+cmd+p>` to open the command pallet. Then type `install plugin`. Look for the `DeSelect` plugin and select it.

### Usage

Select one or more files or directories and press **`<shift>+x`**. The files and directories should be de-selected now.

There is also a command `invert all` that will invert the selection for every file in the current pane. Therefore, if you have a few files selected, then this command will unselect those few and select all the others.

### Features

 - It simply removes the current selection from the files in the pane.
 - The `invert all` command will invert all file selections.
