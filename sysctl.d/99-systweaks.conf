# By default the kernel likes to swap often and a lot.
# The following lines make swapping an last effort to prevent the OOM killer instead of swapping for no reason
vm.swappiness=10
vm.dirty_ratio = 10
vm.dirty_background_ratio = 5
vm.vfs_cache_pressure = 50

# Enabling the "magic key" that can save systems... for those with IBM keyboards
kernel.sysrq=1

# TCP "fastopen"
net.ipv4.tcp_fastopen = 3

# TCP optimiziation for modern WAN links
net.core.rmem_default = 1048576
net.core.rmem_max = 16777216
net.core.wmem_default = 1048576
net.core.wmem_max = 16777216
net.core.optmem_max = 65536
net.ipv4.tcp_rmem = 4096 1048576 2097152
net.ipv4.tcp_wmem = 4096 65536 16777216
net.ipv4.udp_rmem_min = 8192
net.ipv4.udp_wmem_min = 8192

# TCP keepalives from ~2h is the standard. Setting it to a more reasonable value...
net.ipv4.tcp_keepalive_time = 60
net.ipv4.tcp_keepalive_intvl = 10
net.ipv4.tcp_keepalive_probes = 6

# Lowering MTU probing for a more stable connection
net.ipv4.tcp_mtu_probing = 1

# Just because one connection was slow doesn't mean others will be slow as well
net.ipv4.tcp_slow_start_after_idle = 0

# Disable ICMP redirects. ICMP redirects are rarely used but can be used in
# MITM (man-in-the-middle) attacks. Disabling ICMP may disrupt legitimate
# traffic to those sites.
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0

# Turn on Source Address Verification in all interfaces to prevent some
# spoofing attacks
net.ipv4.conf.default.rp_filter=1
net.ipv4.conf.all.rp_filter=1

# Do not accept IP source route packets (we are not a router)
net.ipv4.conf.default.accept_source_route=0
net.ipv4.conf.all.accept_source_route=0
net.ipv6.conf.default.accept_source_route=0
net.ipv6.conf.all.accept_source_route=0

# No redirects.
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

# Ignore bogus ICMP errors
net.ipv4.icmp_echo_ignore_broadcasts=1
net.ipv4.icmp_ignore_bogus_error_responses=1
net.ipv4.icmp_echo_ignore_all=0

# Don't log Martian Packets (impossible packets)
net.ipv4.conf.default.log_martians=0
net.ipv4.conf.all.log_martians=0

